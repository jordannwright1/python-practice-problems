# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    if value1 > value2 and value3:
        print(f"{value1} is the maximum number")
    elif value2 > value1 and value3:
        print(f"{value2} is the maximum number")
    if value3 > value1 and value2:
        print(f"{value3} is the maximum number")
    else:
        print(value1 or value2 or value3)


max_of_three(2, 4, 6)
