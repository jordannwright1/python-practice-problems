# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# define two variables: forward_spelling, reverse_spelling
# maybe use if statements to specify conditions
# make a boolean condition such that if both variables are equal to each other
# ...then (is_palindrome) returns True
# new keywords for review
def is_palindrome(word):
    word = word.lower().replace(' ', '')
    reversed_string = ''.join(reversed(word))
    if word == reversed_string:
        print("is a palidrome")
    else:
        print("is not a palidrome")


is_palindrome("racecar")
