# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
# logic behind this seems to be to arbitrarily set max value equal to
# first index.
# if any item in the list is greater than this value, set that equal
# to max value, so it becomes the new max value.
# repeat for every item in the list until the end.
# finally return the max value.
def max_in_list(values):
    if len(values) == 0:
        return None
    max_value = values[0]
    for item in values:
        if item > max_value:
            max_value = item
    return max_value
