# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.
# input: a list of (potential) keys
# output: a list of values
# want to make it so if the key is in both the key list and the dictionary
# then it appends the corresponding value to the new list and
# finally returns the new list

def translate(key_list, dictionary):
    list = []
    for key in key_list:
        list.append(dictionary.get(key))
    return list


print(translate(["name", "age"], {"name": "Noor", "age": 29}))
