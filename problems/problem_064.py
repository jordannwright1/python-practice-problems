# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]
# create empty list
# need to find a way to combine the high and low list (each item)
# then need to loop through the combined list and find the difference between each item

#
# def temperature_differences(highs, lows):
#     combined_list = zip(highs, lows)
#     lst = list(combined_list)
#     for item in lst:

def temperature_differences(lst1, lst2):
    the_list = []
    for item1, item2 in zip(lst1, lst2):
        the_list.append(item1 - item2)
        if len(lst1) != len(lst2):
            return None



# print(temperature_differences([80, 81, 75, 80], [72, 78, 70, 70] ))
