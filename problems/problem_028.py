# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.
# For this one you're creating an empty string and telling the function to
# add an item if it is not in the new variable.
# Otherwise it would not add the item, so there are no repeats.

def remove_duplicate_letters(s):
    new_s = []
    for item in s:
        if item not in new_s:
            new_s.append(item)
    return new_s


print(remove_duplicate_letters(""))

# Another solution
# for item in s:
#     if item not in new_s:
#         new_s += item
# return new_s
