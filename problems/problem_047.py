# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    has_lower = False
    has_upper = False
    has_digit = False
    has_specialc = False
    for character in password:
        if character.isalpha():
            if character.islower():
                has_lower = True
            else:
                has_upper = True
        elif character.isdigit():
            has_digit = True
        elif character == "$" or character == "!" or character == "@":
            has_specialc = True

    return (len(password) >= 6
            and len(password) <= 12
            and has_lower
            and has_upper
            and has_digit
            and has_specialc
        )
