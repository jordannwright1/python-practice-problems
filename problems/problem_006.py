# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18:
        print(f"{age} is old enough to skydive.")
    else:
        print("Sorry, you are not old enough to skydive.")
    if has_consent_form is True:
        print("Consent form has been signed.")
    else:
        print("Consent form not signed.")


can_skydive(21, False)
